FROM golang:1.15.7 AS image_builder

WORKDIR /opt/telegram_bot_movies
COPY . /opt/telegram_bot_movies

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags="-w -s -X 'main.BuildDate=$(date --rfc-3339=ns)'" -o bot ./src

ENTRYPOINT ["./bot"]



FROM alpine:3.13.1 as image_compact

WORKDIR /opt/telegram_bot_movies

COPY --from=image_builder /opt/telegram_bot_movies/resources ./resources
COPY --from=image_builder /opt/telegram_bot_movies/bot .

CMD ["./bot"]

