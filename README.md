# TELEGRAM_BOT_MOVIES

* Author: Alexander Myasnikov
* mailto: myasnikov.alexander.s@gmail.com
* git: https://gitlab.com/amyasnikov/telegram_bot_movies



## Description

* telegram bot @amyasnikov_movies_bot
* Guess the movie from the photo



## Build && Run

```
docker build -t telegram_bot_movies .
```

```
docker run --rm   \
  --env TELEGRAM_BOT_MOVIES_TOKEN=XXXXXXXXXX:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   \
  --env TELEGRAM_BOT_MOVIES_MOVIES=/opt/telegram_bot_movies/resources/movies.json   \
  --env TELEGRAM_BOT_MOVIES_STATS=/tmp/tg_stats   \
  telegram_bot_movies
```



