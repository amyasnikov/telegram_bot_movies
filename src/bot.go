package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"runtime"
	"sort"
	"time"

	api "gopkg.in/tucnak/telebot.v2"
	"gopkg.in/yaml.v2"
)

var BuildDate = "unknown"

type Stats struct {
	BuildDate   string
	CurrentDate string
	Memory      map[string]string
	Quizzes     int
}

type Quiz struct {
	Photo           string
	Question        string
	Options         []string
	CorrectOptionId int
}

type Movie struct {
	Name          string   `json:"name"`
	Photos        []string `json:"photos"`
	MoreLikeNames []string `json:"more_like_names"`
}

type Movies struct {
	Movies []Movie `json:"movies"`
}

type Tgbot struct {
	Config Config
	Movies Movies
	Stats  Stats
}

func (bot *Tgbot) initConfig() {
	bot.Config = Config{}
	bot.Config.init()

	fmt.Printf("config: \n%v\n", bot.Config.show())
}

func (bot *Tgbot) initMovies() {
	file, err := os.Open(bot.Config.Movies)

	if err != nil {
		panic("Can not load movies")
	}

	decoder := json.NewDecoder(file)
	decoder.Decode(&bot.Movies)
}

func (bot *Tgbot) init() {

	rand.Seed(time.Now().UTC().UnixNano())

	bot.initConfig()
	bot.initMovies()
}

func (bot *Tgbot) run() {

	go func() {
		for true {
			bot.updateStats()
			bot.saveStats()
			time.Sleep(1 * time.Minute)
		}
	}()

	b, err := api.NewBot(api.Settings{
		Token:  bot.Config.Token,
		Poller: &api.LongPoller{Timeout: time.Duration(bot.Config.Sleep) * time.Millisecond},
	})

	if err != nil {
		fmt.Printf("error: %v\n", err)
		return
	}

	b.Handle("/start", func(m *api.Message) {
		bot.processStart(b, m)
	})

	b.Handle("Next question", func(m *api.Message) {
		bot.processStart(b, m)
	})

	b.Handle("/help", func(m *api.Message) {
		bot.processHelp(b, m)
	})

	b.Handle("/stats", func(m *api.Message) {
		bot.processStats(b, m)
	})

	b.Handle(api.OnText, func(m *api.Message) {
		bot.process_unknown_command(b, m)
	})

	b.Start()
}

func (bot *Tgbot) processStart(b *api.Bot, m *api.Message) {
	quiz := bot.getMoviesQuiz()

	menuQuiz := &api.ReplyMarkup{ResizeReplyKeyboard: true}
	btnQuizNext := menuQuiz.Text("Next question")

	menuQuiz.Reply(
		menuQuiz.Row(btnQuizNext),
	)

	p := &api.Photo{File: api.FromURL(quiz.Photo)}
	b.SendAlbum(m.Chat, api.Album{p}, menuQuiz)

	poll := &api.Poll{
		Type:          api.PollQuiz,
		Question:      quiz.Question,
		CorrectOption: quiz.CorrectOptionId,
	}

	for _, o := range quiz.Options {
		poll.AddOptions(o)
	}

	b.Send(m.Chat, poll, menuQuiz)

	bot.Stats.Quizzes++
}

func (bot *Tgbot) processHelp(b *api.Bot, m *api.Message) {
	text := "hello, " + m.Sender.FirstName + "\n"
	text += "Try /start for start quiz."
	b.Send(m.Chat, text)
}

func (bot *Tgbot) process_unknown_command(b *api.Bot, m *api.Message) {
	text := "Sorry. I don't know this command. Try /help."
	b.Send(m.Chat, text)
}

func (bot *Tgbot) processStats(b *api.Bot, m *api.Message) {
	data, _ := yaml.Marshal(&bot.Stats)
	b.Send(m.Chat, string(data))
}

func (bot *Tgbot) updateStats() {
	bot.Stats.BuildDate = BuildDate
	bot.Stats.CurrentDate = time.Now().String()
	bot.Stats.Memory = make(map[string]string)

	var memStats runtime.MemStats
	runtime.ReadMemStats(&memStats)

	bot.Stats.Memory["Alloc"] = ByteCountIEC(memStats.Alloc)
	bot.Stats.Memory["TotalAlloc"] = ByteCountIEC(memStats.TotalAlloc)
	bot.Stats.Memory["Sys"] = ByteCountIEC(memStats.Sys)
	bot.Stats.Memory["Lookups"] = ByteCountIEC(memStats.Lookups)
	bot.Stats.Memory["Mallocs"] = ByteCountIEC(memStats.Mallocs)
	bot.Stats.Memory["Frees"] = ByteCountIEC(memStats.Frees)
	bot.Stats.Memory["HeapAlloc"] = ByteCountIEC(memStats.HeapAlloc)
	bot.Stats.Memory["HeapSys"] = ByteCountIEC(memStats.HeapSys)
	bot.Stats.Memory["HeapIdle"] = ByteCountIEC(memStats.HeapIdle)
	bot.Stats.Memory["HeapInuse"] = ByteCountIEC(memStats.HeapInuse)
	bot.Stats.Memory["HeapReleased"] = ByteCountIEC(memStats.HeapReleased)
	bot.Stats.Memory["HeapObjects"] = ByteCountIEC(memStats.HeapObjects)
	bot.Stats.Memory["StackInuse"] = ByteCountIEC(memStats.StackInuse)
	bot.Stats.Memory["StackSys"] = ByteCountIEC(memStats.StackSys)
	bot.Stats.Memory["MSpanInuse"] = ByteCountIEC(memStats.MSpanInuse)
	bot.Stats.Memory["MSpanSys"] = ByteCountIEC(memStats.MSpanSys)
	bot.Stats.Memory["MCacheInuse"] = ByteCountIEC(memStats.MCacheInuse)
	bot.Stats.Memory["MCacheSys"] = ByteCountIEC(memStats.MCacheSys)
	bot.Stats.Memory["BuckHashSys"] = ByteCountIEC(memStats.BuckHashSys)
	bot.Stats.Memory["GCSys"] = ByteCountIEC(memStats.GCSys)
	bot.Stats.Memory["OtherSys"] = ByteCountIEC(memStats.OtherSys)
	bot.Stats.Memory["NextGC"] = ByteCountIEC(memStats.NextGC)
	bot.Stats.Memory["LastGC"] = ByteCountIEC(memStats.LastGC)
	bot.Stats.Memory["PauseTotalNs"] = ByteCountIEC(memStats.PauseTotalNs)
	bot.Stats.Memory["NumGC"] = fmt.Sprintf("%v", memStats.NumGC)
	bot.Stats.Memory["NumForcedGC"] = fmt.Sprintf("%v", memStats.NumForcedGC)
	bot.Stats.Memory["GCCPUFraction"] = fmt.Sprintf("%.6f", memStats.GCCPUFraction)
	bot.Stats.Memory["EnableGC"] = fmt.Sprintf("%v", memStats.EnableGC)
}

func (bot *Tgbot) saveStats() {
	data, _ := yaml.Marshal(&bot.Stats)
	ioutil.WriteFile(bot.Config.Stats, []byte(data), 0644)
}

func (bot *Tgbot) getMoviesQuiz() Quiz {
	quiz := Quiz{}

	movies := bot.Movies.Movies

	movie := movies[rand.Intn(len(movies))]

	quiz.Options = append(quiz.Options, movie.Name)

	for i := 0; i < min(bot.Config.OptionsSimilarNames, len(movie.MoreLikeNames)); i++ {
		name := movie.MoreLikeNames[rand.Intn(len(movie.MoreLikeNames))]
		contains := containsString(quiz.Options, name)
		if !contains {
			quiz.Options = append(quiz.Options, name)
		}
	}

	for len(quiz.Options) < bot.Config.OptionsNames {
		movie := movies[rand.Intn(len(movies))]
		name := movie.Name
		contains := containsString(quiz.Options, name)
		if !contains {
			quiz.Options = append(quiz.Options, name)
		}
	}

	sort.Strings(quiz.Options)

	quiz.Photo = movie.Photos[rand.Intn(len(movie.Photos))]
	quiz.Question = "What is the name of the movie?"
	quiz.CorrectOptionId = findString(quiz.Options, movie.Name)

	return quiz
}
