package main

import (
	"encoding/json"
	"os"
	"strconv"
)

type Config struct {
	Token  string
	Movies string
	Stats  string
	Sleep  uint64

	OptionsSimilarNames int
	OptionsNames        int
}

func (config *Config) init() {
	if config.Token = os.Getenv("TELEGRAM_BOT_MOVIES_TOKEN"); config.Token == "" {
		panic("TELEGRAM_BOT_MOVIES_TOKEN is empty")
	}

	if config.Movies = os.Getenv("TELEGRAM_BOT_MOVIES_MOVIES"); config.Movies == "" {
		config.Movies = "/opt/telegram_bot_movies/resources/movies.json"
	}

	if config.Stats = os.Getenv("TELEGRAM_BOT_MOVIES_STATS"); config.Stats == "" {
		config.Stats = "/tmp/telegram_bot_movies_stats.yaml"
	}

	if sleep := os.Getenv("TELEGRAM_BOT_MOVIES_SLEEP"); sleep == "" {
		config.Sleep = 1000
	} else {
		config.Sleep, _ = strconv.ParseUint(sleep, 10, 64)
	}

	config.OptionsSimilarNames = 4
	config.OptionsNames = 7
}

func (config *Config) show() string {
	json, _ := json.MarshalIndent(config, "", "  ")
	return string(json)
}
